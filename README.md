DOM To JSON
===========

Renders a DOM table to a JSON object.

# Changelog

### v0.1.5
 - Transfered the repository in the jillix account

### v0.1.3
 - Bind v0.2.1, Events v0.1.8

### v0.1.2
 - Fixed the problem with the filters.

### v0.1.1
 - emit `ready`
 - Implemented filters
