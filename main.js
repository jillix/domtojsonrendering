'use strict';

var Bind = require('github/jillix/bind');
var Events = require('github/jillix/events');

module.exports = function (config, dataContext) {

    var self = this;

    Events.call(self, config);

    var changeCache = [];

    function setDropdown () {
        $("[data-change]:visible").each(function () {
            var $this = $(this);

            if (changeCache.indexOf($this) === -1) {
                changeCache.push($this);
                updateFilters($this, false);
            }
        });
    }

    setDropdown();

    // dropdown on change
    $("[data-change]").on("change", function () {
        updateFilters($(this), true);
    });

    var $current = $(".current", self.dom);
    // delete the sellected row
    $(self.dom).on("click", ".remove", function () {
        var row = $(this).parent().parent();
        row.remove();

        var obj = collectData();
        self.emit("changed", obj);
    });

    // add a new row
    $(self.dom).on("click", ".add", function () {
         var template = $(".template", self.dom)
                        .clone()
                        .removeClass("template");

        var tds = [];

        template.find("td").each(function () {
            tds.push($(this));
        });

        var i = -1;
        var valid = true;
        var firstInvalid;


        $current.find("input").each(function () {
            var $this = $(this);
            var val = $this.val();

            if (!val && valid) {
                valid = false;
                firstInvalid = $this;
            }

            tds[++i].text(val);
        });

        if (!valid) {
            firstInvalid.focus();
            return;
        }

        $current.before(template);
        $current.find("input").val("");
        $current.find("input").first().focus();

        var obj = collectData();
        self.emit("changed", obj);
    });

    $("input", self.dom).on("keyup", function (e) {
        if (e.keyCode === 13) {
            $(".add", self.dom).click();
        }
    });

    self.empty = function () {
        $("tbody tr", self.dom).not(".template, .current").remove();
    };

    self.build = function (from, obj) {

        setDropdown();

        if (typeof from === "string") {
            obj = $("[data-update='" + from + "']").val();
            try { obj = JSON.parse(obj); } catch (e) { return; }
        }

        self.empty();

        var keys = [];
        for (var key in obj) {
            keys.push(key);
        }

        for (var i = 0; i < obj[keys[0]].length; ++i) {
            var template = $(".template", self.dom)
                            .clone()
                            .removeClass("template");

            var index = -1;

            template.find("td[data-value]").each(function () {
                var $this = $(this);

                var val = obj[keys[++index]][i];

                var filterIn = $this.attr("data-filter-in");
                if (filterIn) {
                    var filterFunction = findFunction(window, filterIn);
                    if (typeof filterFunction === "function") {
                        val = filterFunction(self, undefined, undefined, val);
                        $this.text(val);
                    }
                }

                $this.text(val);
            });

            $current.before(template);
        }
    };

    // build the object
    // {
    //      "data-key1": [ ... ],
    //      "data-key2": [ ... ]
    // }
    function collectData () {
        var obj = {};

        $("[data-key]", self.dom).each(function () {
            var key = $(this).attr("data-key");
            obj[key] = [];

            $("[data-value='" + key + "']:not('input')", self.dom).each(function () {
                var $this = $(this);
                var value = $this.text();

                var filterIn = $this.attr("data-filter-out");
                var filterFunction = findFunction(window, filterIn);
                if (typeof filterFunction === "function") {
                    value = filterFunction(self, undefined, undefined, value);
                }

                obj[key].push(value);
            });
        });

        // remove template
        for (var key in obj) {
            obj[key].splice(0, 1);
        }

        return obj;
    }

    function updateFilters(elem, emit) {
        var $that = $(elem);
        var $selected = $that.find(":selected")

        var filterIn = $selected.attr("data-filter-in") || "";
        var filterOut = $selected.attr("data-filter-out") || "";
        var filterChange = $selected.attr("data-filter-change");

        $($that.attr("data-change")).each(function () {
            var $this = $(this);

            var oldFilterIn = $this.attr("data-filter-in");
            var oldFilterOut = $this.attr("data-filter-out");

            // change data-filter attributes
            $this.attr("data-filter-in", filterIn);
            $this.attr("data-filter-out", filterOut);

            var text = $this.text();
            var filterFunction = findFunction(window, filterChange);
            if (typeof filterFunction === "function") {
                var options = {
                    value: text,
                    oldFilterIn: oldFilterIn,
                    oldFilterOut: oldFilterOut
                };
                text = filterFunction(self, undefined, undefined, options);
            }

            $this.text(text);
        });

        if (emit) {
            var obj = collectData();
            self.emit("changed", obj);
        }
    }

    self.emit("ready", config);
};


function findValue (parent, dotNot) {

    if (!dotNot) return undefined;

    var splits = dotNot.split('.');
    var value;

    for (var i = 0; i < splits.length; i++) {
        value = parent[splits[i]];
        if (value === undefined) return undefined;
        if (typeof value === 'object') parent = value;
    }

    return value;
}

function findFunction (parent, dotNot) {

    var func = findValue(parent, dotNot);

    if (typeof func !== 'function') {
        return undefined;
    }

    return func;
}

